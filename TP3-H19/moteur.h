#ifndef __MOTEUR__
#define __MOTEUR__

#include <vector>

#include "piston.h"

class Moteur {
	private:
        int numero_serie;
    protected:
		std::vector<Piston*> pistons;
	public:
		Moteur(int numero_serie);
		int getNbPistons();
        int getNumeroSerie();
		~Moteur();

        // Fonction virtuelle pure
        virtual int getPuissance()=0;
};

#endif
